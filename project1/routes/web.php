<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('addData', function () {
    $demo = new App\demo();
    $demo->string1 = " data1";
    $demo->string2 = " data2";
    $demo->save();
    echo 'đã thêm data';
})->name('adddata');
Route::get('/addData/{string1}/{string2}', function ($string1,$string2) {
    $demo = new App\demo();
    $demo->string1 = $string1;
    $demo->string2 = $string2;
    $demo->save();
    echo 'đã thêm data';
})->name('adddata2');

//get view
Route :: get('add','MyControl@getAdd')->name('getAdd');

//get view
Route :: get('show','MyControl@getShow')->name('getShow');

//get view
Route :: get('index','MyControl@getIndex')->name('getIndex');



