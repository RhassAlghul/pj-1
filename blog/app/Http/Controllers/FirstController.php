<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FirstController extends Controller
{
    //make controller : php artisan make:controller + name

    public function getController(){
        echo 'hello world, first controller';
    }

    public function getController2($x, $y){
        $sum = $x + $y;
        echo 'hello world, first controller ' . $sum;
    }

//    public function getView(){
//        $data['user'] = 'mydata';
//        return view('MyFirstView',$data);
//    }

    public function getView(){
        $data['user'] = [0, 3, -1, 4, -5];
        return view('MyFirstView',$data);
    }

    public function getView1(){
        return view('view1');
    }

    public function getView2(){
        return view('view2');
    }

    public function getHome(){
        return view('Home');
    }

    public function getDemo(){
        return view('demo3');
    }
}
